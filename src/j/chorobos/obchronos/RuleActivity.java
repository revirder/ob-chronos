package j.chorobos.obchronos;



import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class RuleActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rule);
		
		MyView View = new MyView(this);
		setContentView(View);
	}

	
}


class MyView extends View{
	
	private Bitmap Bitmap;
	
	public MyView(Context context){
		super(context);
		setFocusable(true);
		
		//Resourceインスタンスの生成
		Resources res = this.getContext().getResources();
		//画像の読み込み
		Bitmap = BitmapFactory.decodeResource(res, R.drawable.rule);
		
	}
		//描画処理
		@Override
		protected void onDraw(Canvas canvas){
			super.onDraw(canvas);
			
			//説明描画
			Paint Paint = new Paint();
			canvas.drawBitmap(Bitmap, 0, 0,Paint);
		}
		
	}
