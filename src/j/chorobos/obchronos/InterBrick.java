package j.chorobos.obchronos;

import android.R.integer;
import android.graphics.RectF;

public class InterBrick {
	int num = 0;
	RectF rect;
	int range=1,brank=2,m=1;
	float cx=0,cy=0;
	
	public InterBrick(int num,RectF player){
		switch(num){
			case 1:
				this.num = num;
				rect = new RectF(player.left-range,player.top-range,player.left,player.top);
				break;
				
			case 2:
				this.num = num;
				rect = new RectF(player.left+m*brank,player.top-range,player.right-m*brank,player.top);
				break;
				
			case 3:
				this.num = num;
				rect = new RectF(player.right,player.top-range,player.right+range,player.top);
				break;
				
			case 4:
				this.num = num;
				rect = new RectF(player.right,player.top+m*brank,player.right+range,player.bottom-m*brank);
				break;
				
			case 5:
				this.num = num;
				rect = new RectF(player.right,player.bottom,player.right+range,player.bottom+range);
				break;
				
			case 6:
				this.num = num;
				rect = new RectF(player.left+m*brank,player.bottom,player.right-m*brank,player.bottom+range);
				break;
				
			case 7:
				this.num = num;
				rect = new RectF(player.left-range,player.bottom,player.left,player.bottom+range);
				break;
				
			case 8:
				this.num = num;
				rect = new RectF(player.left-range,player.top+m*brank,player.left,player.bottom-m*brank);
				break;
		}
	}
	
	public void update(RectF player){
		switch(num){
		case 1:
			rect.set(player.left-range,player.top-range,player.left,player.top);
			break;
			
		case 2:
			rect.set(player.left+m*brank,player.top-range,player.right-m*brank,player.top);
			break;
			
		case 3:
			rect.set(player.right,player.top-range,player.right+range,player.top);
			break;
			
		case 4:
			rect.set(player.right,player.top+m*brank,player.right+range,player.bottom-m*brank);
			break;
			
		case 5:
			rect.set(player.right,player.bottom,player.right+range,player.bottom+range);
			break;
			
		case 6:
			rect.set(player.left+m*brank,player.bottom,player.right-m*brank,player.bottom+range);
			break;
			
		case 7:
			rect.set(player.left-range,player.bottom,player.left,player.bottom+range);
			break;
			
		case 8:
			rect.set(player.left-range,player.top+m*brank,player.left,player.bottom-m*brank);
			break;
		}
	}
}
