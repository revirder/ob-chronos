package j.chorobos.obchronos;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;

public class Brick {
	float w,h,haba;
	Paint p,p1,p2;
	ImageBrick imageBrick;
	ImageBackGround imageBackGround;
	ArrayList<RectF> rectf_list;
	ArrayList<BrickInfo> brickInfo;
	Stage stageInfo;
	int time = 0;
	int[][] brick={{0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,1,1,2,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,2,1,3,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,1,2,1,3,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0}};
	
	int[][] group={{0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,1,1,1,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,3,3,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,2,2,2,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0},
				   {0,0,0,0,0,0,0,0}};
	
	public Brick(Context context,float w,float h,int stage){
		this.w = w;
		this.h = h-100;
		haba = w/10;
		p = new Paint();
		p1 = new Paint();
		p1.setColor(Color.RED);
		p2 = new Paint();
		p2.setColor(Color.GRAY);
		rectf_list = new ArrayList<RectF>();
		imageBrick = new ImageBrick(context.getResources(),(int)w,(int)h);
		imageBackGround = new ImageBackGround(context.getResources(),(int)w,(int)h);
		stageInfo = new Stage(context,stage);
		stageInfo.setStage(stage);
		brick = stageInfo.getStageInfoOfBrick();
		group = stageInfo.getStageInfoOfGroup();
		time = stageInfo.getTime();
		brickInfo = new ArrayList<BrickInfo>();
	}
	
	// ブロック回転(時計回り90°)
	public void Rotate(){
		List<Block> blockList = new ArrayList<Block>();
		
		// 軸取得ループ
		for( int nCntY = 0 ; nCntY < 12 ; nCntY++ ){
			for( int nCntX = 0 ; nCntX < 8 ; nCntX++ ){

				// 軸を探索
				if( brick[nCntY][nCntX] == 2 ){
					
					// 軸のグループIDと位置を取得
					Block block = new Block();
					block.groupID = group[nCntY][nCntX];
					block.axis.x = nCntX;
					block.axis.y = nCntY;
					
					// リストに保存
					blockList.add(block);
				}
			}
		}

		// ブロック関係取得ループ
		for( int nCntY = 0 ; nCntY < 12 ; nCntY++ ){
			for( int nCntX = 0 ; nCntX < 8 ; nCntX++ ){
				
				// 軸の周囲にある回転部のブロックを探索
				if( brick[nCntY][nCntX] == 1 ){
					
					// ブロックのグループIDを取得
					int groupID = group[nCntY][nCntX];
					
					// 同じグループの軸に関連付ける(axisのraidリストに登録)
					for( int cntAxis = 0 ; cntAxis < blockList.size(); cntAxis++ ){
						Block axis = blockList.get(cntAxis);
						if( groupID == axis.groupID ){
							axis.raid.add( new Point( nCntX, nCntY ) );
							break;
						}
					}
				}
			}
		}

		// マップデータ配列・グループID配列をクリア
		for( int nCntY = 0 ; nCntY < 12 ; nCntY++ ){
			for( int nCntX = 0 ; nCntX < 8 ; nCntX++ ){
				brick[nCntY][nCntX] = 0;
				group[nCntY][nCntX] = 0;
			}
		}
		
		// ブロック回転ループ
		for( int cntAxis = 0 ; cntAxis < blockList.size(); cntAxis++ ){
			Block axis = blockList.get(cntAxis);
			
			// 軸をマップに登録
			brick[axis.axis.y][axis.axis.x] = 2;
			group[axis.axis.y][axis.axis.x] = axis.groupID;
			
			// 連結ブロックを回転してマップに登録
			for( int cntRaid = 0 ; cntRaid < axis.raid.size(); cntRaid++ ){
				Point raid = axis.raid.get(cntRaid);
				int subX = raid.x - axis.axis.x;
				int subY = raid.y - axis.axis.y;
				brick[axis.axis.y + subX][axis.axis.x - subY] = 1;
				group[axis.axis.y + subX][axis.axis.x - subY] = axis.groupID;
			}
		}
	}
	
	public ArrayList<RectF> drawBrickRect(boolean b,Canvas canvas){
		rectf_list.clear();
		brickInfo.clear();
		RectF r = new RectF();
		int i=0,n=0;
		if(b){
			for(i=11;i>=0;i--){
				for(n=7;n>=0;n--){
					switch(brick[i][n]){
					case 0:
						break;
						
					case 1:
						r = new RectF((float)(w-haba*(9-n)),(float)(h-haba*(12-i)),(float)(w-haba*(8-n)),(float)(h-haba*(11-i)));
						brickInfo.add(new BrickInfo(r,group[i][n]));
						canvas.drawBitmap(imageBrick.getBrickBitmap(0),new Rect(0,0,(int)haba,(int)haba),r,p1);
						rectf_list.add(r);
						break;
						
					case 2:
						r = new RectF((float)(w-haba*(9-n)),(float)(h-haba*(12-i)),(float)(w-haba*(8-n)),(float)(h-haba*(11-i)));
						brickInfo.add(new BrickInfo(r,group[i][n]));
						canvas.drawBitmap(imageBrick.getBrickBitmap(1),new Rect(0,0,(int)haba,(int)haba),r,p1);
						rectf_list.add(r);
						break;
					}
				}
			}
		}
		
		return rectf_list;
	}
	
	public int[][] getMapData(){
		return brick;
	}
	
	public int[][] getGroup(){
		return group;
	}
	
	public ArrayList<BrickInfo> getBrickInfo(){
		return brickInfo;
	} 
	
	public int getTime(){
		return time;
	}

	public void drawBackGround(Canvas canvas){
		canvas.drawBitmap(imageBackGround.getBackgroundBitmap(0),0,0,p1);
	}
}
