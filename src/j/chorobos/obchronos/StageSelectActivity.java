package j.chorobos.obchronos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class StageSelectActivity extends Activity {

	//リストに設定するメインテキスト
		private final String[] Stage = new String[]{
				"STAGE 1",
				"STAGE 2",
				"STAGE 3"
				};
		
		//リストに設定するサブテキスト
		private final String[] Score = new String[]{
				"ハイスコア "+"100",
				"ハイスコア "+"120",
				"ハイスコア "+"90"
		};
		
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stage_select);
		//ListViewに表示させる内容
				final List<Map<String,String>> list = new ArrayList<Map<String,String>>();
				 for (int i=0; i<Stage.length; i++) {
			            Map<String, String> map = new HashMap<String, String>();
			            map.put("stage", Stage[i]);
			            map.put("score", Score[i]);
			            list.add(map);
			        }
			
				 //ListViewに設定するデータを生成
				 SimpleAdapter adapter = new SimpleAdapter(
			                this,
			                list,
			                android.R.layout.simple_list_item_2,
			                new String[] {"stage", "score"},
			                new int[] {android.R.id.text1, android.R.id.text2}
			                );


			ListView listView = (ListView)findViewById(R.id.listView1);
			listView.setAdapter(adapter);
			
			//アイテムクリック時のイベントを設定
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				public void onItemClick(AdapterView<?> parent,View view, int pos, long id){
					Intent i = new Intent(getApplicationContext(), GameActivity.class);
				//押された時の処理	
					switch(pos){
					case 0:
						startActivity(i);
							//描画データを送る処理
					break;
					
					case 1:
						startActivity(i);
							//描画データを送る処理
						break;
						
					case 2:
						startActivity(i);
							//描画データを送る処理
						break;
					}
				}
				});
			}
	
}
