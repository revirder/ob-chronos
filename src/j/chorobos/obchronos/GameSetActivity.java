package j.chorobos.obchronos;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class GameSetActivity extends Activity implements OnClickListener{
	Intent i,j,k;
	ImageButton button1;
	ImageButton button2;
	ImageButton button3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_set);
		boolean clear=true;
	
		i = new Intent(GameSetActivity.this,GameActivity.class);
		j = new Intent(GameSetActivity.this,StageSelectActivity.class);
		k = new Intent(GameSetActivity.this,MainActivity.class);
		
		button1 = (ImageButton)findViewById(R.id.button1);
		button1.setOnClickListener(this);
	
		button2 = (ImageButton)findViewById(R.id.button2);
		button2.setOnClickListener(this);
	
		button3 = (ImageButton)findViewById(R.id.button3);
		button3.setOnClickListener(this);
		
		Intent intent = getIntent();
		clear = intent.getBooleanExtra("clear",true);
		
		ImageView imageView = (ImageView) findViewById(R.id.image);
		
		if(clear){
			imageView.setBackgroundResource(R.drawable.clear);
		}
		else if(!clear){
			imageView.setBackgroundResource(R.drawable.gameover);
		}
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button1:
			startActivity(i);
			break;
		case R.id.button2:
			startActivity(j);
			break;
		case R.id.button3:
			startActivity(k);
		}
		
	}
}
