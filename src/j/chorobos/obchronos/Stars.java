//=====================================
//
//	Stars.java
//	Author	:	TakumaAkagawa
//
//=====================================
package j.chorobos.obchronos;

// インポート
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

// クラス定義
public class Stars {

	// プロパティ
	private int numStar, getStarNum;
	private List<RectF> rectList = new ArrayList<RectF>();

	// コンストラクタ
	public Stars( int mapData[][], int numX, int numY, float mapTop, float blockSize ){

		numStar = 0;
		
		// 星の位置を探索
		for( int cntY = 0 ; cntY < numY ; cntY++ ){
			for( int cntX = 0 ; cntX < numX ; cntX++ ){

				// 星があるかどうか
				if( mapData[cntY][cntX] == 3 ){
					
					// 星のあるタイルの矩形を計算
					RectF rect = new RectF();
					rect.top = mapTop + cntY * blockSize;
					rect.bottom = rect.top + blockSize;
					rect.left = blockSize * ( cntX + 1 );
					rect.right = blockSize * ( cntX + 2 );
					
					// リストに登録
					rectList.add( rect );
					numStar++;
				}
			}
		}
	}
	
	// 判定
	public boolean run( Canvas canvas, RectF recChar ){

		boolean clearFlag = false;
		
		// ☆リストの矩形と当たり判定を取る
		for( int cntStar = 0 ; cntStar < rectList.size(); cntStar++ ){
			RectF recStar = rectList.get(cntStar);

			// 矩形と点の当たり判定
			if( RectF.intersects(recStar, recChar) ){
				
				// TODO:SE再生
				
				
				// リストから☆を削除
				rectList.remove(cntStar);
				
				// すべての☆が回収されたらtrueを返す
				if( rectList.size() == 0 ){
					clearFlag = true;
				}
			}
		}
		
		// TODO:Bitmapへ差し替え
		// 現在の入手数を表示
		Paint paint = new Paint();
		for( int cntStar = 0; cntStar < numStar  ; cntStar++ ){
			if(cntStar < rectList.size() + 1 ){
				paint.setColor(0xff888800);
			}
			else{				
				paint.setColor(0xffffff00);
			}
			canvas.drawCircle(200 + cntStar * 60, 100, 25, paint);				
		}
		
		// リストの矩形を描画
		paint.setColor(Color.YELLOW);
		for( int cntStar = 0 ; cntStar < rectList.size() ; cntStar++ ){
			RectF rect = rectList.get(cntStar);
			canvas.drawRect(rect, paint);
		}

		// ゲームクリア判定を返す
		return clearFlag;
	}
}

// end of file