package j.chorobos.obchronos;

import java.util.ArrayList;
import java.util.List;
import android.graphics.Point;

public class Block {

	public int groupID;
	public Point axis;
	public List<Point> raid;
	
	public Block(){
		axis = new Point();
		raid = new ArrayList<Point>();
	};
}
