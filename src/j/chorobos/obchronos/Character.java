package j.chorobos.obchronos;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;

public class Character {
	float x=0,y=0,haba,hani=4,speed,char_frm,w,h,rate;
	RectF player,temp,r,center_r;
	int state=0,info=0,player_group=1,n;
	Paint p1;
    ImageChar imageChar;
    ArrayList<RectF> rectf_list;
    int[][] mapData,group;
    Canvas canvas;
    Matrix matrix;
    ArrayList<InterBrick> inter_array;
    int[] inter_group = new int[2];
    boolean[] b = new boolean[9];
	
	public Character(Context context,float w,float h,int i,int n){
		haba = w/10;
		this.w = w;
		this.h = h-100;
		x = haba*n+haba/2;
		y = this.h-12*haba+i*haba-haba/2+1;
		player = new RectF((float) (x-haba/2+1),(float) (y-haba/2+1),(float) (x+haba/2-1),(float) (y+haba/2-1));
		p1 = new Paint();
        Resources res = context.getResources();
		imageChar = new ImageChar(res,(int)w,(int)h);
		speed = haba/35;
		rectf_list = new ArrayList<RectF>();
		r = new RectF();
		mapData = new int[12][8];
		group = new int[12][8];
		center_r = new RectF();
		matrix = new Matrix();
		rate = speed;
        inter_array = new ArrayList<InterBrick>();
		
		for(int in=8;in>=1;in--){
			inter_array.add(new InterBrick(in,player));
		}
	}

	
	public RectF drawChoro(Canvas canvas,ArrayList<RectF> rectf_list,int[][] mapData,int[][] group,ArrayList<BrickInfo> brickInfo){
		this.mapData = mapData;
		this.group = group;
		this.canvas = canvas;
		int i=0;
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		this.rectf_list = rectf_list;
		RectF brick;
		
		ArrayList<BrickInfo> inter_brick = new ArrayList<BrickInfo>();
		ArrayList<InterBrick> inter_rects = new ArrayList<InterBrick>();
		
		for(int n=1;n<9;n++){
			b[n] = false;
		}
		
		//playerのrectの位置によりplayer回りの更新
		for(i=0;i<8;i++){
			inter_array.get(i).update(player);
		}
		
		//player回りのrectがどのbrickのrectと当たっているか
		for(int u=0;u<8;u++){
			for(int p=0;p<brickInfo.size();p++){
				if(RectF.intersects(inter_array.get(u).rect,brickInfo.get(p).rectf)){
					switch(inter_array.get(u).num){
						case 1:
							b[1] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 2:
							b[2] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 3:
							b[3] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 4:
							b[4] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 5:
							b[5] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 6:
							b[6] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 7:
							b[7] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
							
						case 8:
							b[8] = true;
							inter_brick.add(brickInfo.get(p));
							inter_rects.add(inter_array.get(u));
							break;
					}
				}
			}
		}
		
		/*1 2 3
		  8   4  
		  7 6 5*/
		
		if(notInterAnotherGroup(inter_brick)){
			move(b);
		}
		else if(!notInterAnotherGroup(inter_brick)){
			brick = getBrickInfoRectf(inter_brick,inter_rects);
			if((b[8]&b[6])|(b[6]&b[4])|(b[4]&b[2])|(b[2]&b[8])|!(b[8]&b[4])|!(b[6]&b[2])){
 				move(b);
 				player_group = inter_group[1];
 			}
		}
		
		player.set((float) (x-haba/2+1),(float) (y-haba/2+1),(float) (x+haba/2-1),(float) (y+haba/2-1));
		//for(i=0;i<8;i++){canvas.drawRect(inter_array.get(i).rect,paint);}
		drawChar(canvas);
		
		return player;
	}
	
	public void deFlag(ArrayList<BrickInfo> inter_brick,ArrayList<InterBrick> inter_rects){
		BrickInfo brickInfo=null;
		InterBrick interBrick=null;
		int i=0;
		
		for(i=0;i<inter_brick.size();i++){
			brickInfo = inter_brick.get(i);
			interBrick = inter_rects.get(i);
			if(brickInfo.group!=player_group){
				b[interBrick.num]=false;
			}
		}
	}
	
	public boolean notInterAnotherGroup(ArrayList<BrickInfo> inter_brick){
		boolean b = true;
		int n = 0;
		inter_group[0] = player_group;
		
		//現在のgroupとは違うgroupの検出
		for(n=1;n<inter_brick.size();){
			if(player_group==inter_brick.get(n).group){
				n++;
			}
			else if(player_group!=inter_brick.get(n).group){
				inter_group[1] = inter_brick.get(n).group;
				break;
			}
		}
		
		for(int i=0;i<inter_brick.size();i++){
			b = b & (player_group==inter_brick.get(i).group);
		}
		
		return b;
	}
	
	public void drawChar(Canvas canvas){
		Bitmap bitmap;
		Matrix m = new Matrix();
		int f=0;
		if(char_frm < 12) {f = 0;speed=rate;}
		else if(char_frm < 18) {f = 1;speed=rate/2;}
		else if(char_frm < 24) {f = 2;speed=rate/3;}
		else if(char_frm < 36) {f = 3;speed=rate/4;}
		else if(char_frm < 42) {f = 2;speed=rate/5;}
		else f = 1;
		if(char_frm == 42) char_frm = 0;
		if(b[2]){
			m.postRotate(180,haba/2,haba/2);
		}
		else if(b[4]){
			m.postRotate(-90,haba/2,haba/2);
		}
		else if(b[6]){
			m.postRotate(0,haba/2,haba/2);
		}
		else if(b[8]){
			m.postRotate(90,haba/2,haba/2);
		}
		bitmap = Bitmap.createBitmap(imageChar.getCharacterBitmap(f),0,0,(int)haba,(int)haba, m, true);
		canvas.drawBitmap(bitmap,new Rect(0,0,(int)haba,(int)haba),player,p1);
		char_frm++;
	}
	
	public void rotate(){
		float cx = 0,cy = 0,radial = 0,offset_angle = 0,X = 0,Y = 0;
		
		info = player_group;
		
		hana: for(int i = 0 ; i < 12 ; i++){
			for(int n = 0 ; n < 8 ; n++){
				if(info==group[i][n]&mapData[i][n]==2){
					cx = (float)((n+1)*haba) + haba/2;
					cy = (float)(h-12*haba+i*haba) + haba/2;
					
					break hana;
				}
			}
		}
		X = (this.x-cx);
		Y = (this.y-cy);
		
		radial = (float)Math.sqrt(X*X+Y*Y);
		
		if(Y>=0){
			offset_angle = (float)Math.acos(X/radial);
		}
		else if(Y<0){
			offset_angle = -(float)Math.acos(X/radial);
		}

		this.x = (float) (cx + radial*Math.cos(offset_angle+Math.PI/2));
		this.y = (float) (cy + radial*Math.sin(offset_angle+Math.PI/2));
		
		player.set((float) (x-haba/2+1),(float) (y-haba/2+1),(float) (x+haba/2-1),(float) (y+haba/2-1));
	}
	
	public RectF getBrickInfoRectf(ArrayList<BrickInfo> inter_brick,ArrayList<InterBrick> inter_rects){
		int i=0;
		BrickInfo brick = null;
		
		for(i=0;i<inter_rects.size();i++){
			brick = inter_brick.get(i);
			if(brick.group==player_group){
				break;
			}
		}
		
		return brick.rectf;
	}
	
	public void move(boolean[] b){
		if(b[8]&b[7]&b[6]){
			x+=speed;
		}
		else if(b[6]&b[5]&b[4]){
			y-=speed;
		}
		else if(b[4]&b[3]&b[2]){
			x-=speed;
		}
		else if(b[2]&b[1]&b[8]){
			y+=speed;
		}
		
		else if(b[5]&b[6]&b[7]){
			x+=speed;
		}
		else if(b[1]&b[8]&b[7]){
			y+=speed;
		}
		else if(b[1]&b[2]&b[3]){
			x-=speed;
		}
		else if(b[3]&b[4]&b[5]){
			y-=speed;
		}
		
		else if(b[6]&b[7]){
			x+=speed;
		}
		else if(b[1]&b[8]){
			y+=speed;
		}
		else if(b[2]&b[3]){
			x-=speed;
		}
		else if(b[4]&b[5]){
			y-=speed;
		}
		
		else if(b[8]){
			y+=speed;
		}
		else if(b[6]){
			x+=speed;
		}
		else if(b[4]){
			y-=speed;
		}
		else if(b[2]){
			x-=speed;
		}
		
		else if(b[7]){
			y+=speed;
		}
		else if(b[1]){
			x-=speed;
		}
		else if(b[3]){
			y-=speed;
		}
		else if(b[5]){
			x+=speed;
		}
	}
	
	public boolean isDead(){
		boolean b=false;
		int player_x=0,player_y=0;
		if(n>=100){
			player_x = (int)(x/haba) - 1;
			player_y = (int)((y-(h-12*haba))/haba);
			if(mapData[player_y][player_x]!=0){
				b = true;
			}
			else{
				b = false;
			}
		}
		n++;
		return b;
	}
}
