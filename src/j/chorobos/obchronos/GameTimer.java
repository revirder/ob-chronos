//=====================================
//
//	GameTimer.java
//	Author	:	TakumaAkagawa
//
//=====================================
package j.chorobos.obchronos;

// インポート
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

// クラス定義
public class GameTimer{
	
	// プロパティ
	private long time;		// 残り時間
	private long last;		// 前回更新時間
	Paint paint;			// ペイント
	
	// コンストラクタ
	public GameTimer( long maxTime ){
		time = maxTime * 1000;
		last = 0;
		
		// ペイント
		paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setTextSize( 62 );
	}

	// 更新・描画
	public boolean run(Canvas canvas){
		
		// 継続判定
		if( time <= 0 ){
			return false;
		}
		
		// タイム更新
		if( last != 0 ){
			// 経過時間計算
			long passed = android.os.SystemClock.uptimeMillis() - last;
			
			// タイム減算
			time -= passed;
		}
		
		// タイム描画
		canvas.drawText("Time:" + time / 1000, 0, 100, paint);

		// 更新時間取得
		last = android.os.SystemClock.uptimeMillis();
		
		// 終了判定
		if( time <= 0 ){
			return true;
		}
		else{
			return false;
		}
	}
}

// end of file