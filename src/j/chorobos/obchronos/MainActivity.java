package j.chorobos.obchronos;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;

public class MainActivity extends Activity implements OnClickListener{
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Display dis;
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		dis = wm.getDefaultDisplay();
		int w = dis.getWidth();
		int h = dis.getHeight();
		TitleView View = new TitleView(this,w,h);
		setContentView(View);
		View view = getLayoutInflater().inflate(R.layout.activity_main, null);
		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		//addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		/*
		setContentView(View);
		
		setContentView(R.layout.activity_main);
		*/
		ImageButton button1 = (ImageButton)findViewById(R.id.imageButton1);
		button1.setOnClickListener(this);

		ImageButton button2 = (ImageButton)findViewById(R.id.imageButton2);
		button2.setOnClickListener(this);
		/*
		ImageView imageView = new ImageView(this);
		imageView.setBackgroundResource(R.id.imageView2);
		setContentView(imageView);
		*/
		View im_v3 = (View)findViewById(R.id.imageView2);
		
		
		ScaleAnimation scale = new ScaleAnimation(
				0.5f,
				2.0f,
				1.0f,
				1.0f,
				160.0f,
				46.0f
		);
		
		scale.setDuration(1000);
		scale.setInterpolator(new CycleInterpolator(0.5f));	
		im_v3.startAnimation(scale);
		Stage stage = new Stage(getBaseContext(),0);
	}

	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.imageButton1:
			Intent i = new Intent(this, StageSelectActivity.class);
			startActivity(i);
			break;
		case R.id.imageButton2:
			Intent k = new Intent(this, RuleActivity.class);
			startActivity(k);
			break;
		}
		ScaleAnimation scale = new ScaleAnimation(
				0.5f,
				5.0f,
				0.5f,
				5.0f,
				v.getWidth() / 2,
				v.getHeight() / 2
				);
		scale.setDuration(200);
		scale.setInterpolator(new CycleInterpolator(0.5f));	
		v.startAnimation(scale);
	}
}
class TitleView extends View{
	
	private Bitmap title;
	
	public TitleView(Context context,int w,int h){
		super(context);
		setFocusable(true);
		//Resourceインスタンスの生成
		Resources res = this.getContext().getResources();
		//画像の読み込み
		title = BitmapFactory.decodeResource(res, R.drawable.title);	
	    title = Bitmap.createScaledBitmap(title, w,h, true);
	}
		//描画処理
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		//タイトル描画


		Paint Paint = new Paint();
		canvas.drawBitmap(title, 0, 0,Paint);
	}
		
}
