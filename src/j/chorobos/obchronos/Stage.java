package j.chorobos.obchronos;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.R.integer;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.LogWriter;
import android.util.Log;

public class Stage {
	SharedPreferences first;
	FileOutputStream fos;
	String stage_str,stage1="\n",stage2="\n",stage3="\n",stage4="\n",stage5="\n",stage6="\n",stage7="\n",stage8="\n",stage9="\n",stage10="\n",stage11="\n",stage12="\n",stage13="\n",stage14="\n",stage15="\n",stage16="\n",stage17="\n",stage18="\n",stage19="\n",stage20="\n",stage21="\n",stage22="\n",stage23="\n",stage24="\n",stage25="\n",highScore;
	int stage_int;
	FileInputStream fis;
	BufferedReader reader;
	Context context;
	String[] stage_info_str = null;
	String line="";
	int stage_i=0,stage;
	
	public Stage(Context context,int stage){
		this.context = context;
		first = context.getSharedPreferences("first",Context.MODE_PRIVATE);
		first.edit().putBoolean("first",true).commit();
		if(first.getBoolean("first",true)){
			//登録するステージ情報をつめた文字列の初期化　0~95ブロック情報96~191グループ情報
			stage1 = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"+","
			        +"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"+","+"30\n";
			highScore = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
			stage_str = stage1 
					  + stage2 
					  + stage3 
					  + stage4 
					  + stage5 
					  + stage6 
					  + stage7 
					  + stage8 
					  + stage9 
					  + stage10 
					  + stage11 
					  + stage12 
					  + stage13 
					  + stage14 
					  + stage15 
					  + stage16 
					  + stage17 
					  + stage18 
					  + stage19 
					  + stage20 
					  + stage21 
					  + stage22 
					  + stage23 
					  + stage24 
					  + stage25 
					  + highScore;
			
			//stage.txtに保存
			try {
				fos = context.openFileOutput("stage.txt",Context.MODE_PRIVATE);
				fos.write(stage_str.getBytes());
				Log.w("first","j");
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			first.edit().putBoolean("first",false).commit();
		}
		
		
		try {
			fis = context.openFileInput("stage.txt");
			reader = new BufferedReader(new InputStreamReader(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//カンマで区切ってStringの配列につめる
		while(stage_i<stage){
			try {
				if((line = reader.readLine())!=null){
					stage_info_str = line.split(",");
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			stage_i++;
		}
	}
	
	public int getTime(){
		int time=0;
		
		time = Integer.valueOf(stage_info_str[192]);
		
		return time;
	}
	
	public int[][] getStageInfoOfBrick(){
		int[] stageInfo1d = new int[96];
		int[][] stageInfo2d = new int[12][8];
		int i1=0;
		
		//String配列をint配列へ
		for(int i=0;i<96;i++){
			stageInfo1d[i] = Integer.valueOf(stage_info_str[i]);
		}
		
		//１次元配列を２次元配列へ
		for(int y=0;y<12;y++){
			for(int x=0;x<8;x++){
				stageInfo2d[y][x] = stageInfo1d[i1];
				i1++;
			}
		}
		
		return stageInfo2d;
	}
	
	public int[][] getStageInfoOfGroup(){
		int[] stageInfo1d = new int[96];
		int[][] stageInfo2d = new int[12][8];
		int i1=0;
		
		//String配列をint配列へ
		for(int i=0;i<96;i++){
			stageInfo1d[i] = Integer.valueOf(stage_info_str[i+96]);
		}
		
		//１次元配列を２次元配列へ
		for(int y=0;y<12;y++){
			for(int x=0;x<8;x++){
				stageInfo2d[y][x] = stageInfo1d[i1];
				i1++;
			}
		}
		
		return stageInfo2d;
	}

	//引数のstageは1~
	public void setStage(int stage){
		int stage_i=0;
		
		this.stage = stage;
		try {
			fis = context.openFileInput("stage.txt");
			reader = new BufferedReader(new InputStreamReader(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//カンマで区切ってStringの配列につめる
		while(stage_i<stage){
			try {
				if((line = reader.readLine())!=null){
					stage_info_str = line.split(",");
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			stage_i++;
		}
	}
	
	public void setHighScore(int stage,int highScore){
		String line="",setString="",scoreString="";
		String[] highScoreStrings=new String[25];
		
		try {
			fis = context.openFileInput("stage.txt");
			reader = new BufferedReader(new InputStreamReader(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//stage情報をsetString，ハイスコアをlineからhighScoreStrings
		try {
			for(int i=0;i<25;i++){
				setString = setString + reader.readLine() + "\n";
			}
			line = reader.readLine();
			highScoreStrings = line.split(",");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//指定のステージのハイスコア更新
		highScoreStrings[stage - 1] = Integer.toString(highScore);
		scoreString=highScoreStrings[0];
		for(int i=1;i<highScoreStrings.length;i++){
			scoreString = scoreString + "," + highScoreStrings[i];
		}
		
		setString += scoreString;
		
		//保存
		try {
			fos = context.openFileOutput("stage.txt",Context.MODE_PRIVATE);
			fos.write(setString.getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//引数stageは1~
	public String getHighScoreString(int stage){
		String line="";
		String[] highScoreStrings=new String[25];
		
		try {
			fis = context.openFileInput("stage.txt");
			reader = new BufferedReader(new InputStreamReader(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//カンマで区切ってStringの配列につめる
		try {
			while((line = reader.readLine())!=null){
				highScoreStrings = line.split(",");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return highScoreStrings[stage - 1];
	}
}
