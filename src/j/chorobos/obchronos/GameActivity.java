package j.chorobos.obchronos;

import java.util.ArrayList;




import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;



public class GameActivity extends Activity implements EndListener{
		TestSurface ts;
		int w,h;
		WindowManager wm;
		Display dis;
		final int GAME_OVER = 0;
		final int CLEAR = 1;
		Intent intent;
		 //int in = 0;//^--------------------------------------
		//Bitmap t = BitmapFactory.decodeResource(getResources(),R.drawable.player2);

		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
			dis = wm.getDefaultDisplay();
			w = dis.getWidth();
			h = dis.getHeight();
			ts = new TestSurface(getBaseContext(),w,h,1);
			ts.setEndListener(this);
			setContentView(ts);
			
			intent = new Intent(GameActivity.this,GameSetActivity.class);
		}
		
		@Override
		public boolean onCreateOptionsMenu(Menu menu){
			//getMenuInflater().inflate(R.menu.game, menu);
			Intent intent = new Intent(GameActivity.this,PauseActivity.class);
			startActivity(intent);
			return false;
		} 

		@Override
		public void onResume(){
			super.onResume();
			ts.setLoop(true);
		}
		
		@Override
		public void onDestroy(){
			super.onDestroy();
			ts.endloop();
		}
		@Override
		public void onPause(){
			super.onPause();
			ts.setLoop(false);
		}
		@Override
		public void clear(){
			intent.putExtra("clear",true);
			startActivity(intent);
		}
		@Override
		public void gameOver(){
			intent.putExtra("clear",false);
			startActivity(intent);
		}

		//setContentView(R.layout.activity_game);
		
	/*	//Buttonを取
				Button button1 = (Button)findViewById(R.id.button1);
				button1.setOnClickListener(this);
				
				Button button2 = (Button)findViewById(R.id.button2);
				button2.setOnClickListener(this);
		*/		
	
	//SurfaceViewの設定
	//TestSurface mSurfaceView = new TestSurface(this, 480, 800);
	//setContentView(mSurfaceView);

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}*/
}
	
	
/*	@Override
	public void onClick(View v) {
		
		Intent i = new Intent(this,GameSetActivity.class);
		
		switch(v.getId()){
		case R.id.button1:
			//クリアデータの送信
			startActivity(i);
			break;
		case R.id.button2:
			//クリアデータの送信
			startActivity(i);
			break;
		}
		
	}
*/
