package j.chorobos.obchronos;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

//
public class TestSurface extends SurfaceView implements SurfaceHolder.Callback,Runnable{
	SurfaceHolder holder;
	Thread thread;
	RectF player;
	boolean loop=true;
	float Maptop;
	Context context;
	double speed=1,haba=0,w=0,h=0;
	GameTimer gt;
	Stars star;
	Character character;
	Brick brick;
	EndListener el;
	PowerManager pm;
	WakeLock wl;
	
	public TestSurface(Context context,int w,int h,int stage){
		super(context);
		holder = getHolder();
		holder.addCallback(this);
		
		this.w = w;
		this.h = h-100;
		this.context = context;
		haba = w/10;
		Maptop = (float)(this.h-12*haba);
		this.context = context;
		player = new RectF();
		character = new Character(context,w,h,3,3);
		brick = new Brick(context,w,h,stage);
		gt = new GameTimer(brick.getTime());
		star = new Stars(brick.getMapData(),8,12,Maptop,(float)haba);
		pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My tag");
	}
	
	public TestSurface(Context context,AttributeSet attrs,int w,int h,int stage){
		super(context,attrs);
		holder = getHolder();
		holder.addCallback(this);
		
		this.w = w;
		this.h = h-100;
		this.context = context;
		haba = w/10;
		Maptop = (float)(this.h-12*haba);
		this.context = context;
		player = new RectF();
		character = new Character(context,w,h,3,3);
		brick = new Brick(context,w,h,stage);
		gt = new GameTimer(brick.getTime());
		star = new Stars(brick.getMapData(),8,12,Maptop,(float)haba);
		pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My tag");
	}

	@Override
	public void run() {
		Canvas canvas;
		for(;;){
			if(loop){
				canvas = holder.lockCanvas();
				canvas.drawColor(Color.WHITE);
				brick.drawBackGround(canvas);
				player = character.drawChoro(canvas,brick.drawBrickRect(true,canvas),brick.getMapData(),brick.getGroup(),brick.getBrickInfo());
				if(gt.run(canvas)){
					//TODO:時間切れの処理
					el.gameOver();
				}
				if(star.run(canvas,player)){
					//TODO:clearのときの処理
					el.clear();
				}
				if(character.isDead()){
					el.gameOver();
				}
				holder.unlockCanvasAndPost(canvas);
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		thread = new Thread(this);
		thread.start();
		wl.acquire();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		wl.release();
		synchronized(thread){
			loop = false;
		}
		try{
			thread.join();
		}catch(InterruptedException e){
			thread.interrupt();
		}
	}
	
	public void endloop(){
		wl.release();
		synchronized(thread){
			loop = false;
		}
		try{
			thread.join();
		}catch(InterruptedException e){
			thread.interrupt();
		}
	}
	
	public void setLoop(boolean loop){
		this.loop = loop;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e){
		switch(e.getAction()){
		case MotionEvent.ACTION_UP:
			character.rotate();
			brick.Rotate();
			
			break;
		}
		return true;
	}
	
	public void setEndListener(EndListener el){
		this.el = el;
	}
}
