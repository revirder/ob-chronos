package j.chorobos.obchronos;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageBrick {
	float w,h,haba;
	Bitmap[] b_brick = new Bitmap[10];
	
	public ImageBrick(Resources res,int w,int h){
		this.w = w;
		this.h = h-100;
		this.haba = w/10;
		b_brick[0] = BitmapFactory.decodeResource(res, R.drawable.block1);
		b_brick[0] = Bitmap.createScaledBitmap(b_brick[0], (int)haba, (int)haba, false);
		b_brick[1] = BitmapFactory.decodeResource(res, R.drawable.block0);
		b_brick[1] = Bitmap.createScaledBitmap(b_brick[1], (int)haba, (int)haba, false);
		b_brick[2] = BitmapFactory.decodeResource(res, R.drawable.block3);
		b_brick[2] = Bitmap.createScaledBitmap(b_brick[2], (int)haba, (int)haba, false);
		b_brick[3] = BitmapFactory.decodeResource(res, R.drawable.block2);
		b_brick[3] = Bitmap.createScaledBitmap(b_brick[3], (int)haba, (int)haba, false);
	}
	public Bitmap getBrickBitmap(int i){
		return b_brick[i];
	}
}
