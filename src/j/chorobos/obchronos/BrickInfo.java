package j.chorobos.obchronos;

import android.graphics.RectF;

public class BrickInfo {
	RectF rectf;
	int group=0;
	
	public BrickInfo(RectF rectf,int group){
		this.rectf = rectf;
		this.group = group;
	}
}
