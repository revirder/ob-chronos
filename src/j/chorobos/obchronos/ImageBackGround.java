package j.chorobos.obchronos;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageBackGround {
	float w,h,haba;
	Bitmap[] b_background = new Bitmap[5];
	
	public ImageBackGround(Resources res,int w,int h){
		this.w = w;
		this.h = h-100;
		this.haba = w/10;
		b_background[0] = BitmapFactory.decodeResource(res, R.drawable.background1);
		b_background[0] = Bitmap.createScaledBitmap(b_background[0], (int)this.w, (int)h, false);
	}
	public Bitmap getBackgroundBitmap(int i){
		return b_background[i];
	}
}