package j.chorobos.obchronos;

import java.util.EventListener;

public interface EndListener extends EventListener {
	public void clear();
	public void gameOver();
}
