package j.chorobos.obchronos;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageChar {
	float w,h,haba;
	Bitmap[] b_char = new Bitmap[20];
	Bitmap[] b_brick = new Bitmap[10];
	Bitmap[] b_background = new Bitmap[5];
	
	public ImageChar(Resources res,int w,int h){
		this.w = w;
		this.h = h-100;
		this.haba = w/10;
		b_char[0] = BitmapFactory.decodeResource(res, R.drawable.char0);
		b_char[0] = Bitmap.createScaledBitmap(b_char[0], (int)haba, (int)haba, false);
		b_char[1] = BitmapFactory.decodeResource(res, R.drawable.char1);
		b_char[1] = Bitmap.createScaledBitmap(b_char[1], (int)haba, (int)haba, false);
		b_char[2] = BitmapFactory.decodeResource(res, R.drawable.char2);
		b_char[2] = Bitmap.createScaledBitmap(b_char[2], (int)haba, (int)haba, false);
		b_char[3] = BitmapFactory.decodeResource(res, R.drawable.char3);
		b_char[3] = Bitmap.createScaledBitmap(b_char[3], (int)haba, (int)haba, false);
		b_char[4] = BitmapFactory.decodeResource(res, R.drawable.char4);
		b_char[4] = Bitmap.createScaledBitmap(b_char[4], (int)haba, (int)haba, false);
		b_char[5] = BitmapFactory.decodeResource(res, R.drawable.char5);
		b_char[5] = Bitmap.createScaledBitmap(b_char[5], (int)haba, (int)haba, false);
		b_char[6] = BitmapFactory.decodeResource(res, R.drawable.char6);
		b_char[6] = Bitmap.createScaledBitmap(b_char[6], (int)haba, (int)haba, false);
		b_char[7] = BitmapFactory.decodeResource(res, R.drawable.char7);
		b_char[7] = Bitmap.createScaledBitmap(b_char[7], (int)haba, (int)haba, false);
		b_char[8] = BitmapFactory.decodeResource(res, R.drawable.char8);
		b_char[8] = Bitmap.createScaledBitmap(b_char[8], (int)haba, (int)haba, false);
		b_char[9] = BitmapFactory.decodeResource(res, R.drawable.char9);
		b_char[9] = Bitmap.createScaledBitmap(b_char[9], (int)haba, (int)haba, false);
		b_char[10] = BitmapFactory.decodeResource(res, R.drawable.char10);
		b_char[10] = Bitmap.createScaledBitmap(b_char[10], (int)haba, (int)haba, false);
		b_char[11] = BitmapFactory.decodeResource(res, R.drawable.char11);
		b_char[11] = Bitmap.createScaledBitmap(b_char[11], (int)haba, (int)haba, false);
		
	}
	public Bitmap getCharacterBitmap(int i){
		return b_char[i];
	}
}
